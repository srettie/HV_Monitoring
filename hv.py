import ROOT
import sys
import os
import array

import datetime as dt

ROOT.gROOT.SetBatch(1)

debug = False

nChan = 4

def getTimeDiff(t1, t2):
    year1 = int(t1[0:4])
    month1 = int(t1[5:7])
    day1 = int(t1[8:10])

    year2 = int(t2[0:4])
    month2 = int(t2[5:7])
    day2 = int(t2[8:10])

    h1 = int(t1[11:13])
    m1 = int(t1[14:16])
    s1 = int(t1[17:19])

    h2 = int(t2[11:13])
    m2 = int(t2[14:16])
    s2 = int(t2[17:19])

    
    dt1 = dt.datetime(year1, month1, day1, h1, m1, s1)
    dt2 = dt.datetime(year2, month2, day2, h2, m2, s2)
    
    diff = (dt2-dt1).total_seconds()

    return diff/(3600)
    

def plot_hv(log, zoom):
    
    name = log.split('/')[len(log.split('/'))-1]
    
    with open(log) as f:
        lines = f.readlines()

    ref_time = lines[0].split(': ')[0].replace('[','').replace(']','')

    if debug:
        print 'ref_time: ' + ref_time

    time_I = {}
    currents = {}
    time_V = {}
    voltages = {}
    
    for i_chan in range(0, nChan):
        time_I[i_chan] = []
        currents[i_chan] = []
        time_V[i_chan] = []
        voltages[i_chan] = []

    for l in lines:
        time = l.split(': ')[0].replace('[','').replace(']','')
        time_diff = getTimeDiff(ref_time, time)
        ch = l.split('ch [')[1][0]
        if ('IMonH' in l or 'IMonL' in l):
            current = float(l.split('val')[1].replace('[', '').replace(']', '').replace(';', ''))

            currents[int(ch)].append(current)
            time_I[int(ch)].append(time_diff)
            if debug:
                print '\nIMon[H/L] ch'+ch+' line: ' + l.replace('\n', '')
                print 'time: ' + time
                print 'ch: ' + ch
                print 'Appended time_diff = ' + str(time_diff)
                print 'Appended current = ' + str(current)                
        
        if 'VMon' in l:
            volt = float(l.split('val')[1].replace('[', '').replace(']', '').replace(';', ''))

            voltages[int(ch)].append(volt)
            time_V[int(ch)].append(time_diff)
            if debug:
                print '\nVMon ch'+ch+' line: ' + l.replace('\n', '')
                print 'time: ' + time
                print 'ch: ' + ch
                print 'Appended time_diff = ' + str(time_diff)
                print 'Appended voltage = ' + str(volt)
                    
    cnv = ROOT.TCanvas()

    suffix = name.replace('.log','')
    if zoom == True:
        suffix += '_zoom'

    for i_ch in range(0, nChan):
        if len(time_I[i_ch]) > 0:
            tg_I = ROOT.TGraph(len(time_I[i_ch]), array.array('d', time_I[i_ch]), array.array('d', currents[i_ch]))
            tg_I.SetTitle('IMon (ch'+str(i_ch)+'): '+suffix)
            tg_I.GetYaxis().SetTitle('Current (#muA)')
            tg_I.GetXaxis().SetTitle('Time from ' +ref_time + ' (h)')
            if zoom:
                # 2 -- y-axis
                tg_I.SetMaximum(tg_I.GetMean(2) + tg_I.GetRMS(2))
                tg_I.SetMinimum(tg_I.GetMean(2) - tg_I.GetRMS(2))
            tg_I.Draw('ALP')
            cnv.SaveAs('plots/'+suffix+'.pdf(')
        if len(time_V[i_ch]) > 0:
            tg_V = ROOT.TGraph(len(time_V[i_ch]), array.array('d', time_V[i_ch]), array.array('d', voltages[i_ch]))
            tg_V.SetTitle('VMon (ch'+str(i_ch)+'): ' + suffix)
            tg_V.GetYaxis().SetTitle('Voltage (V)')
            tg_V.GetXaxis().SetTitle('Time from ' + ref_time + ' (h)')
            if zoom:
                # 2 -- y-axis
                tg_V.SetMaximum(tg_V.GetMean(2) + tg_V.GetRMS(2))
                tg_V.SetMinimum(tg_V.GetMean(2) - tg_V.GetRMS(2))
            tg_V.Draw('ALP')
            cnv.SaveAs('plots/'+suffix+'.pdf(')


    # Save blank last page for now...
    cnv.Clear()
    cnv.SaveAs('plots/'+suffix+'.pdf)')

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print 'Usage: python hv.py <name_of_log_file>'
        sys.exit()

    else:
        if(not os.path.isdir('plots')):
            print 'Making plots directory...'
            os.mkdir('plots')
        for log in sys.argv:
            if log == 'hv.py':
                continue
            print 'Producing plots for ' + log + '...'
            plot_hv(log, False)
            plot_hv(log, True)
